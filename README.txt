$Id
(Draft version)

Allow registered users to claim to be the author or a node. The module will store the history of who have made claims. Site administrators can set who can make claims

To-do:

   1. Which content type's authorship can be claimed
   2. Notify administrator or previous author of the claim
   3. Allow registered users to vote on if a claim is legitimate
   4. Allow registered users to report a spam claim to administrator
